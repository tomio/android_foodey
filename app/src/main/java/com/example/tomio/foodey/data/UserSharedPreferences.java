package com.example.tomio.foodey.data;

import android.content.SharedPreferences;
import android.content.Context;
import android.preference.PreferenceManager;
import android.content.SharedPreferences.Editor;

public class UserSharedPreferences {
    public static final String PREF_USER_ID = "_id";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }


    public static void setUserId(Context context, long id) {
        Editor editor = getSharedPreferences(context).edit();
        editor.putLong(PREF_USER_ID, id);
        editor.commit();
    }


    public static long getUserId(Context context) {
        return getSharedPreferences(context).getLong(PREF_USER_ID, -1);

    }

    public static void deletePreferences(Context context)
    {
        Editor editor = getSharedPreferences(context).edit();
        editor.clear();
        editor.commit();

    }



}
