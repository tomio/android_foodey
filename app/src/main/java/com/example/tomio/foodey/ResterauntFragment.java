package com.example.tomio.foodey;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tomio.foodey.data.ResterauntData;
import com.example.tomio.foodey.data.UserSharedPreferences;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class ResterauntFragment extends Fragment {




    private Button giftCardButton;
    private TextView resterauntTitle;
    private ImageView resterauntImage;
    private TextView resterauntDescription;
    private TextView currentMonth;

    private ImageView phoneButton;
    private ImageView webButton;
    private ImageView mapsButton;
    private ImageView likeButton;
    private int likeStatus = 0;
    private String resterauntLocation;
    private String resterauntPhone;
    private String resterauntWebsite;
    private long resterauntId;
    private long favoriteId;

    private LinearLayout resterauntTitleBackground;
    private View viewContext;
    private static final String TAG = ResterauntFragment.class.getSimpleName();



    public ResterauntFragment()
    {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {


        View view =  inflater.inflate(R.layout.fragment_resteraunt, container, false);

        giftCardButton = (Button) view.findViewById(R.id.gift_card_button);
        resterauntTitle = (TextView) view.findViewById(R.id.resteraunt_title);
        resterauntImage = (ImageView) view.findViewById(R.id.resteraunt_image);
        resterauntDescription =(TextView) view.findViewById(R.id.resteraunt_description);
        phoneButton = (ImageView) view.findViewById(R.id.resteraunt_phone);
        mapsButton = (ImageView) view.findViewById(R.id.resteraunt_directions);
        webButton = (ImageView) view.findViewById(R.id.resteraunt_website);
        likeButton = (ImageView) view.findViewById(R.id.button_like);
        resterauntTitleBackground = (LinearLayout) view.findViewById(R.id.resteraunt_title_background);
        viewContext = view;

        mapsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,

                        Uri.parse("http://maps.google.com/maps?daddr=" + resterauntLocation));
                startActivity(intent);
            }
        });

        phoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String uri = "tel:" + resterauntPhone;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(uri));
                startActivity(intent);
            }
        });

        giftCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getActivity(),BarcodeActivity.class);
                getActivity().startActivity(intent);
            }
        });

        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddFavoriteTask task = new AddFavoriteTask();
                long userId = UserSharedPreferences.getUserId(getActivity());

                    if(likeStatus == 0) {
                        likeStatus = 1;
                        likeButton.setImageResource(R.drawable.like);
                        Log.v(TAG,"The user id for favorites is "+userId);
                        task.execute(userId,resterauntId,Long.parseLong("1"));

                        Toast.makeText(getActivity(),"Added to favorites",Toast.LENGTH_SHORT).show();

                    }
                    else {
                        likeStatus = 0;
                        likeButton.setImageResource(R.drawable.unlike);
                        task.execute(userId, resterauntId, Long.parseLong("-1"),favoriteId);

                    }
            }
        });

        return view;
    }

    public void setCardBackgroundColor(String hexidecimal)
    {
        giftCardButton.setBackgroundColor(Color.parseColor(hexidecimal));

        resterauntTitleBackground.setBackgroundColor(Color.parseColor(hexidecimal));

    }

    public void setResterauntInformation(String[] resterauntInfo)
    {
        resterauntTitle.setText(resterauntInfo[ResterauntData.RESTERAUNT_NAME]);
        resterauntDescription.setText(resterauntInfo[ResterauntData.RESTERAUNT_DESCRIPTION]);
        resterauntLocation = resterauntInfo[ResterauntData.RESTERAUNT_LOCATION];
        resterauntPhone = resterauntInfo[ResterauntData.RESTERAUNT_PHONE];
        resterauntWebsite = resterauntInfo[ResterauntData.RESTERAUNT_WEBSITE];
        resterauntId = Long.parseLong(resterauntInfo[ResterauntData.RESTERAUNT_ID]);
        favoriteId = Long.parseLong(resterauntInfo[ResterauntData.RESTERAUNT_FAVORITE_ID]);

        if(favoriteId > 0)
        {
            likeStatus = 1;
            likeButton.setImageResource(R.drawable.like);
        }

        String imageDataString = resterauntInfo[ResterauntData.RESTERAUNT_IMAGE];



        setResterauntImage(imageDataString);



    }

    private void setResterauntImage(String imageDataString)
    {

        byte[] decodedImage = Base64.decode(imageDataString, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedImage, 0, decodedImage.length);
        resterauntImage.setImageBitmap(decodedByte);

    }


    public class AddFavoriteTask extends AsyncTask<Long,Void,Void> {


        private final String TAG = AddFavoriteTask.class.getSimpleName();


        @Override
        protected Void doInBackground(Long... params)
        {

            ResterauntData dataHelper = new ResterauntData();

            boolean shouldAdd = (params[2] > 0);

            if (shouldAdd)
            {
                favoriteId = dataHelper.addFavoriteResteraunt(params[0],params[1]);
                Log.v(TAG,"The favorite id is"+favoriteId);
            }

            else
            {
                dataHelper.removeFavoriteResteraunt(params[3]);

            }

            return null;

        }

    }



}
