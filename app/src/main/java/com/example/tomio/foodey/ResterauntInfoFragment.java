package com.example.tomio.foodey;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tomio.foodey.data.ResterauntData;
import com.example.tomio.foodey.data.UserSharedPreferences;

import org.w3c.dom.Text;

import java.text.DateFormatSymbols;


public class ResterauntInfoFragment extends Fragment {
    private TextView resterauntTitle;
    private TextView monthTitleText;
    private TextView resterauntDescription;

    private ImageView phoneButton;
    private ImageView webButton;
    private ImageView mapsButton;
    private ImageView resterauntImage;
    private ImageView likeButton;
    private String resterauntLocation;
    private String resterauntPhone;

    private String resterauntWebsite;
    private long resterauntId;
    private long favoriteId;
    private String[] resterauntInfo;
    private boolean shouldDisplayMonthTitle;

    private int likeStatus;

    private final static String TAG = ResterauntInfoFragment.class.getSimpleName();
    public final static String KEY_SHOULD_MONTH_TITLE = "display_month";

    public ResterauntInfoFragment() {    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        resterauntInfo = getArguments().getStringArray(ResterauntData.KEY_RESTERAUNT_INFO);
        shouldDisplayMonthTitle = getArguments().getBoolean(KEY_SHOULD_MONTH_TITLE);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view =  inflater.inflate(R.layout.fragment_resteraunt_info, container, false);

        resterauntTitle =(TextView) view.findViewById(R.id.resteraunt_info_title);
        monthTitleText = (TextView) view.findViewById(R.id.text_month_title_text);

        resterauntDescription = (TextView)  view.findViewById(R.id.resteraunt_info_description);
        phoneButton = (ImageView)  view.findViewById(R.id.resteraunt_info_phone);
        webButton = (ImageView)  view.findViewById(R.id.resteraunt_info_website);
        mapsButton = (ImageView)  view.findViewById(R.id.resteraunt_info_directions);
        likeButton = (ImageView) view.findViewById(R.id.resteraunt_info_button_like);

        if(shouldDisplayMonthTitle)
        {
            setMonth();
        }

        mapsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,

                        Uri.parse("http://maps.google.com/maps?daddr=" + resterauntLocation));
                startActivity(intent);
            }
        });

        phoneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String uri = "tel:" + resterauntPhone;
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(uri));
                startActivity(intent);
            }
        });


        likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddFavoriteTask task = new AddFavoriteTask();
                long userId = UserSharedPreferences.getUserId(getActivity());

                if(likeStatus == 0) {
                    likeStatus = 1;
                    likeButton.setImageResource(R.drawable.like);
                    Log.v(TAG,"The user id for favorites is "+userId);
                    task.execute(userId,resterauntId,Long.parseLong("1"));

                    Toast.makeText(getActivity(), "Added to favorites", Toast.LENGTH_SHORT).show();

                }
                else {
                    likeStatus = 0;
                    likeButton.setImageResource(R.drawable.unlike);
                    task.execute(userId, resterauntId, Long.parseLong("-1"),favoriteId);

                }
            }
        });


        setResterauntInfo();


        return view;

    }


    private void setResterauntInfo()
    {

        resterauntTitle.setText(resterauntInfo[ResterauntData.RESTERAUNT_NAME]);
        resterauntDescription.setText(resterauntInfo[ResterauntData.RESTERAUNT_DESCRIPTION]);
        resterauntLocation = resterauntInfo[ResterauntData.RESTERAUNT_LOCATION];
        resterauntPhone = resterauntInfo[ResterauntData.RESTERAUNT_PHONE];
        resterauntWebsite = resterauntInfo[ResterauntData.RESTERAUNT_WEBSITE];
        resterauntId = Long.parseLong(resterauntInfo[ResterauntData.RESTERAUNT_ID]);
        favoriteId = Long.parseLong(resterauntInfo[ResterauntData.RESTERAUNT_FAVORITE_ID]);

        if(favoriteId > 0)
        {
            likeStatus = 1;
            likeButton.setImageResource(R.drawable.like);
        }

        else
        {
            likeStatus = 0;
        }


        String imageDataString = resterauntInfo[ResterauntData.RESTERAUNT_IMAGE];


    }

    private void setResterauntImage(String imageDataString)
    {

        byte[] decodedImage = Base64.decode(imageDataString, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedImage, 0, decodedImage.length);
        resterauntImage.setImageBitmap(decodedByte);

    }


    public class AddFavoriteTask extends AsyncTask<Long,Void,Void> {


        private final String TAG = AddFavoriteTask.class.getSimpleName();


        @Override
        protected Void doInBackground(Long... params)
        {

            ResterauntData dataHelper = new ResterauntData();

            boolean shouldAdd = (params[2] > 0);

            if (shouldAdd)
            {
                favoriteId = dataHelper.addFavoriteResteraunt(params[0],params[1]);
                Log.v(TAG, "The favorite id is" + favoriteId);
            }

            else
            {
                dataHelper.removeFavoriteResteraunt(params[3]);

            }

            return null;

        }

    }


    private void setMonth()
    {
        monthTitleText.setVisibility(View.VISIBLE);
        int month = Integer.parseInt(resterauntInfo[ResterauntData.RESTERAUNT_MONTH].substring(0,2));
        String year = resterauntInfo[ResterauntData.RESTERAUNT_MONTH].substring(3);
        monthTitleText.setText(new DateFormatSymbols().getMonths()[month-1]+ " "+year);

    }






}
