package com.example.tomio.foodey.data;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * Created by tomio on 3/3/2016.
 */
public class Request
{
    private final String TAG = Request.class.getSimpleName();
    public static final String BASE_URL = "http://159.203.86.41/";

    public String makeRequest(String dataUrl,String requestMethod,String params)
    {

        HttpURLConnection con = null;
        BufferedReader reader = null;
        String jsonString = "";

        try

        {

            Uri uri = Uri.parse(dataUrl);

            String urlString = uri.toString();
            Log.v(TAG, "Url String: " + urlString);


            URL url = new URL(urlString);

            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod( requestMethod);

            if(requestMethod.equals("POST") )
            {

                byte[] postData = params.getBytes( StandardCharsets.UTF_8);
                int dataLength = postData.length;
                con.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
                con.setRequestProperty( "charset", "utf-8");
                con.setRequestProperty( "Content-Length", Integer.toString( dataLength ));


                try( DataOutputStream wr = new DataOutputStream( con.getOutputStream())) {
                    wr.write( postData );

                }
            }


            int responseCode = con.getResponseCode();

            InputStream in = con.getInputStream();
            StringBuffer buffer = new StringBuffer();

            if (in == null) {
                // return null;
                Log.e(TAG, "There was a problem getting the json String");
            }

            reader = new BufferedReader(new InputStreamReader(in));
            String line;

            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");

            }

            if (buffer.length() == 0) {
                return null;

            }
            jsonString = buffer.toString();
            Log.v(TAG, "Json String : " + jsonString);



        }

        catch (IOException e) {
            Log.e(TAG, "Error ", e);
        } finally {
            if (con != null)
                con.disconnect();
        }

        if (reader != null) {
            try {
                reader.close();

            } catch (final IOException e) {
                Log.e(TAG, "There was a problem closing the reader ", e);

            }
        }

        return jsonString;
    }


    public  long parseUserId(String jsonString)
        throws JSONException
    {
        JSONArray userIdArray = new JSONArray(jsonString);
        long userId = userIdArray.getJSONObject(0).getLong("id");
        return userId;

    }

}
