package com.example.tomio.foodey;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.tomio.foodey.data.ResterauntData;
import com.example.tomio.foodey.data.UserSharedPreferences;

public class PastPicksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_picks);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Aesthetic.setBarColor(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        PastPicksTask task =new PastPicksTask();
        long userId = UserSharedPreferences.getUserId(PastPicksActivity.this);
        task.execute(userId);




    }



    public class PastPicksTask extends AsyncTask<Long,Void,String[][]> {


        private ProgressDialog dialog = new ProgressDialog(PastPicksActivity.this);
        private final String TAG = PastPicksTask.class.getSimpleName();


        @Override
        protected void onPreExecute()
        {
            //this.dialog.setMessage("Please wait");
            this.dialog.show();
        }

        @Override
        protected String[][] doInBackground(Long... params) {

            ResterauntData dataHelper = new ResterauntData();
            String[][] resteraunts = dataHelper.getPastPicks(params[0]);
            Log.v(TAG,"The amount of resteraunts "+resteraunts.length);
            return resteraunts;


        }
        @Override
        protected void onPostExecute(String[][] resteraunts)

        {

            if(resteraunts.length > 0)
             {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                 String monthTitle = null;

                 for(int i = 0; i < resteraunts.length; i++)
                {
                    String[] resterauntInfo = resteraunts[i];



                    Bundle bundle = new Bundle();
                    bundle.putStringArray(ResterauntData.KEY_RESTERAUNT_INFO, resterauntInfo);

                    boolean shouldDisplay;


                    if(monthTitle == null || !monthTitle.equals(resterauntInfo[ResterauntData.RESTERAUNT_MONTH].trim()))
                        shouldDisplay = true;
                    else
                        shouldDisplay = false;

                    bundle.putBoolean(ResterauntInfoFragment.KEY_SHOULD_MONTH_TITLE,shouldDisplay);

                    monthTitle =  resterauntInfo[ResterauntData.RESTERAUNT_MONTH].trim();
                    Log.v(TAG,"The month title is "+monthTitle + Boolean.toString(shouldDisplay));

                    ResterauntInfoFragment fragment = new ResterauntInfoFragment();
                    fragment.setArguments(bundle);
                    fragmentTransaction.add(R.id.past_picks_container, fragment);



            }
            fragmentTransaction.commit();


        }
            this.dialog.dismiss();

        }
    }

}
