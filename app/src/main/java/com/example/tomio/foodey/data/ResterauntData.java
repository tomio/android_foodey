package com.example.tomio.foodey.data;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by tomio on 2/20/2016.
 */


public class ResterauntData
{

    public static final int RESTERAUNT_NAME = 1;
    public static final int RESTERAUNT_PHONE = 2;
    public static final int RESTERAUNT_WEBSITE = 3;
    public static final int RESTERAUNT_LOCATION = 4;
    public static final int RESTERAUNT_IMAGE = 5;
    public static final int RESTERAUNT_DESCRIPTION = 0;
    public static final int RESTERAUNT_ID = 6;
    public static final int RESTERAUNT_FAVORITE_ID = 7;
    public static final int RESTERAUNT_MONTH = 8;

    public static final String KEY_RESTERAUNT_INFO = "info";
    private static final String BASE_URL = "http://159.203.86.41/";
    private Request request = new Request();


    private static final String TAG = ResterauntData.class.getSimpleName();

    public void signInUser()
    {
        String params = "email=n&password=n";
        String jsonString = makeRequest(BASE_URL + "signin/", "POST", params);
        Log.v(TAG, "The user json String is " + jsonString);
    }


    public String[][] getPastPicks(long userId)
    {
        String[][] pastPicks = null;
        String dataUrl = Request.BASE_URL+userId+"/past";
        String json = request.makeRequest(dataUrl,"GET",null);
        try
        {
            pastPicks = extractResterauntInfo(json);
        }
        catch (JSONException e )
        {
            Log.e(TAG,"There was an issue getting past pciks",e);

        }

        return pastPicks;
    }


    public long  addFavoriteResteraunt(long userId, long resterauntId)
    {
        long favoriteId = -1;
        String params = "resteraunt_id="+resterauntId+"&user_id="+userId;
        Log.v(TAG,"The favorites params "+params);
        String jsonString = makeRequest(BASE_URL+"favorite/create","POST",params);
        //  Log.v(TAG,"The favorite id string is "+jsonString);
        try
        {
             favoriteId = parseFavoriteId(jsonString);
        }
        catch(JSONException e)
        {
            Log.e(TAG,"There was a problem extractding the favorite id",e);

        }
        return favoriteId;

    }

    public void removeFavoriteResteraunt(long favoriteId)
    {
        String params = "id="+favoriteId;
        Log.v(TAG,"The favorites params "+params);
        makeRequest(BASE_URL+"favorite/delete","POST",params);
    }
    public String[][]  getCurrentRestraunts(long userId)
    {
        Log.v(TAG,"At least got here "+userId );
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/yyyy");
        String month = dateFormat.format(c.getTime());
        Log.v(TAG,BASE_URL+"city/"+userId+"/"+month);
        String jsonString = makeRequest(BASE_URL+"city/"+userId+"/"+month,"GET",null);
        String[][] resteraunts =null;

        try
        {
            resteraunts = extractResterauntInfo(jsonString);
        }
        catch (JSONException e)
        {
            Log.e(TAG, "There was a probmlem extracting from the json String " + e.getMessage(), e);
            e.printStackTrace();
        }


        return resteraunts;
    }


    private String makeRequest(String dataUrl,String requestMethod,String params)
    {

        HttpURLConnection con = null;
        BufferedReader reader = null;
        String jsonString = "";

        try

        {

            Uri uri = Uri.parse(dataUrl);

            String urlString = uri.toString();
            Log.v(TAG, "Url String: " + urlString);


            URL url = new URL(urlString);

            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod( requestMethod);

            if(requestMethod.equals("POST") )
            {

                byte[] postData = params.getBytes( StandardCharsets.UTF_8);
                int dataLength = postData.length;
                con.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
                con.setRequestProperty( "charset", "utf-8");
                con.setRequestProperty( "Content-Length", Integer.toString( dataLength ));


                try( DataOutputStream wr = new DataOutputStream( con.getOutputStream())) {
                    wr.write( postData );

                }
            }


            int responseCode = con.getResponseCode();

            InputStream in = con.getInputStream();
            StringBuffer buffer = new StringBuffer();

            if (in == null) {
                // return null;
                Log.e(TAG, "There was a problem getting the json String");
            }

            reader = new BufferedReader(new InputStreamReader(in));
            String line;

            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");

            }

            if (buffer.length() == 0) {
                return null;

            }
            jsonString = buffer.toString();
            Log.v(TAG, "Json String : " + jsonString);



          }

          catch (IOException e) {
            Log.e(TAG, "Error ", e);
        } finally {
            if (con != null)
                con.disconnect();
        }

        if (reader != null) {
            try {
                reader.close();

            } catch (final IOException e) {
                Log.e(TAG, "There was a problem closing the reader ", e);

            }
        }

        return jsonString;
    }

    public String[][] getFavoritedReserauntsInfo(long userId) {
        String dataUrl = BASE_URL + userId + "/favorites";
        String favoritedJson = makeRequest(dataUrl, "GET", null);
        String[][] resterauntsInfo = null;
        try
        {
             resterauntsInfo =   extractResterauntInfo(favoritedJson);

        }

        catch (JSONException e)
        {
            Log.e(TAG,"There was a problem extacting the favorited json", e);

        }

        return resterauntsInfo;

    }
    public long  signUserUp(String email,String password)
    {
        String dataUrl = BASE_URL+"users";
        String cityId = "1";
        String params = "user[email]="+email+"&user[password]="+password+"&user[city_id]="+cityId;
        Log.v(TAG,params);
        long userId = -1;
        String userJson= makeRequest(dataUrl,"POST",params);

        try
        {
            userId=  parseUserId(userJson);
        }

        catch (JSONException e)
        {
            Log.e(TAG,e.getMessage());
        }
        Log.v(TAG,"The new user_id is "+userJson);

        return userId;
    }

    private long parseUserId(String jsonString)
            throws JSONException
    {
        JSONArray userIdArray = new JSONArray(jsonString);
        long userId = userIdArray.getJSONObject(0).getLong("id");
        return userId;

    }


    private long parseFavoriteId(String jsonString)
            throws JSONException
    {
        JSONArray userIdArray = new JSONArray(jsonString);
        long favoriteId = userIdArray.getJSONObject(0).getLong("favorite_id");
        return favoriteId;
    }


    public static String[][] extractResterauntInfo(String jsonString)
            throws JSONException
    {

        JSONArray resterauntArray= new JSONArray(jsonString);

        String[][] resterauntInfoArray = new String[resterauntArray.length()][9];
        for(int i = 0; i < resterauntArray.length(); i++)
        {


            JSONObject  resterauntInfo = resterauntArray.getJSONObject(i);
            String name = resterauntInfo.getString("name");
            String phone =resterauntInfo.getString("phone");
            String image = resterauntInfo.getString("image");
            String website = resterauntInfo.getString("website");
            String location = resterauntInfo.getString("location");
            String description = resterauntInfo.getString("description");
            String resterauntId = resterauntInfo.getString("resteraunt_id");
            String favoriteId = resterauntInfo.getString("favorite_id");
            String resterauntMonth = resterauntInfo.getString("month");


            String[] resteraunt = resterauntInfoArray[i];
            resteraunt[RESTERAUNT_IMAGE] = image;
            resteraunt[RESTERAUNT_LOCATION] = location;
            resteraunt[RESTERAUNT_NAME]= name;
            resteraunt[RESTERAUNT_PHONE] = phone;
            resteraunt[RESTERAUNT_WEBSITE] = website;
            resteraunt[RESTERAUNT_DESCRIPTION] = description;
            resteraunt[RESTERAUNT_ID] = resterauntId;
            resteraunt[RESTERAUNT_FAVORITE_ID] = favoriteId;
            resteraunt[RESTERAUNT_MONTH] = resterauntMonth;


        }

        return resterauntInfoArray;
   }
}
