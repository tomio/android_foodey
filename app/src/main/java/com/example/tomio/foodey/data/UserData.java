package com.example.tomio.foodey.data;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by tomio on 3/2/2016.
 */
public class UserData {


    private Request requestHelper =  new Request();
    private static final String TAG = UserData.class.getSimpleName();

    public boolean validate(String email,String password, Context context)
    {
        boolean validated = false;
        String message;


        if(email.trim().length() > 0 && password.trim().length() > 0 )
        {


            if(emailIsOkay(email))
            {

            }
            else
            {
                message = "Please enter a valid email address ";
            }

        }

        else
        {
            message = "Please fill out both fields";


        }






        return false;

    }


    private boolean emailIsOkay(String email)
    {

        final Pattern VALID_EMAIL_ADDRESS_REGEX =
              Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(email);

        return matcher.find();
    }
    public long signInUser(String email,String password)
    {
        long userId = -1;
        String dataUrl = requestHelper.BASE_URL+"signin";
        String params = "email="+email+"&password="+password;
        String userJson = requestHelper.makeRequest(dataUrl,"POST",params);

        try
        {
             userId = requestHelper.parseUserId(userJson);
        }
        catch(JSONException e)
        {
            Log.e(TAG,"There was a problem extracting the user id ",e);
        }


        return userId;
    }



    public String[] getUserData(long userId)
    {
      //  String dataUrl = makeRequest(BASE_URL);
      // makeRequest()

        return null;
    }


}
