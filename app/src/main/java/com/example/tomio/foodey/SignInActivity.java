package com.example.tomio.foodey;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tomio.foodey.data.ResterauntData;
import com.example.tomio.foodey.data.UserData;
import com.example.tomio.foodey.data.UserSharedPreferences;

public class SignInActivity extends Activity{
    private TextView applicationTitle;
    private Button signInButton;
    private EditText userEmail;
    private EditText userPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        Aesthetic aesthetic = new Aesthetic();

        applicationTitle = (TextView) findViewById(R.id.sign_in_application_title);
        signInButton = (Button) findViewById(R.id.button_sign_in);
        userEmail = (EditText) findViewById(R.id.field_sign_in_email);
        userPassword = (EditText) findViewById(R.id.field_sign_in_password);

        aesthetic.setBarColor(this);
        applicationTitle.setText(Html.fromHtml(aesthetic.getFoodeyTitleText()));

        signInButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String email = userEmail.getText().toString().trim();
                String password = userPassword.getText().toString().trim();
                SignInTask task = new SignInTask();
                task.execute(email,password);



            }
        });



    }

    public class SignInTask extends AsyncTask<String,Void,Long> {


        private ProgressDialog dialog = new ProgressDialog(SignInActivity.this);


        @Override
        protected void onPreExecute()
        {
            //this.dialog.setMessage("Please wait");
            this.dialog.show();
        }
        private final String TAG = SignInTask.class.getSimpleName();

        @Override
        protected Long doInBackground(String... params) {

            UserData dataHelper = new UserData();

            String email = params[0];
            String password = params[1];

            long userId = dataHelper.signInUser(email,password);


            return userId;


        }
        @Override
        protected void onPostExecute(Long userId)
        {
            this.dialog.dismiss();

            if(userId > 0)
            {
                UserSharedPreferences.setUserId(SignInActivity.this, userId);
                startActivity(new Intent(SignInActivity.this,Home.class));

            }

        }
    }


}
