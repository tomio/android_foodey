package com.example.tomio.foodey;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.tomio.foodey.data.UserSharedPreferences;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

          findViewById(R.id.text_sign_out).setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {

                  UserSharedPreferences.deletePreferences(SettingsActivity.this);
                  startActivity(new Intent(SettingsActivity.this,SignUpActivity.class));
              }
          });

    }

}
