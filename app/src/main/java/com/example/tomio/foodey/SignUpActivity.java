package com.example.tomio.foodey;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tomio.foodey.data.ResterauntData;
import com.example.tomio.foodey.data.UserSharedPreferences;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class SignUpActivity extends Activity {

    private TextView applicationTitle;
    private TextView signInText;

    private EditText emailField;
    private EditText passwordField;
    private Button nextButton;
    private final String TAG = SignUpActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {


        long userId = UserSharedPreferences.getUserId(this);
        if(userId > 0)
        {
            Log.v(TAG,"The user id for the sign in is "+userId);
            Intent intent = new Intent(SignUpActivity.this,Home.class);
            startActivity(intent);
        }


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        emailField = (EditText) findViewById(R.id.field_email);
        passwordField = (EditText) findViewById(R.id.field_password);
        nextButton = (Button) findViewById(R.id.button_next);
        signInText = (TextView) findViewById(R.id.text_sign_in);



        //setBarColor();
        Aesthetic aesthetic = new Aesthetic();
        aesthetic.setBarColor(this);

        applicationTitle = (TextView) findViewById(R.id.application_title);

        applicationTitle.setText(Html.fromHtml(aesthetic.getFoodeyTitleText()));

        nextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailField.getText().toString().trim();
                String password = passwordField.getText().toString().trim();
                String cityId = "1";

                SignUpTask task = new SignUpTask();
                task.execute(email, password);


            }
        });

        signInText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUpActivity.this, SignInActivity.class));
            }
        });





    }


    public class SignUpTask extends AsyncTask<String,Void,Long> {


        private ProgressDialog dialog = new ProgressDialog(SignUpActivity.this);


        @Override
        protected void onPreExecute()
        {
            //this.dialog.setMessage("Please wait");
            this.dialog.show();
        }
        private final String TAG = SignUpTask.class.getSimpleName();

        @Override
        protected Long doInBackground(String... params) {

            ResterauntData dataHelper = new ResterauntData();
            String email = params[0];
            String password = params[1];

           Long userId = dataHelper.signUserUp(email,password);

            Log.v(TAG, "The amount of resteraunts " + userId);
            return userId;


        }
        @Override
        protected void onPostExecute(Long userId)
        {
            this.dialog.dismiss();


            if (userId < 0)
            {
                // the user was not signed up successfully
                Toast.makeText(SignUpActivity.this,"There was a problem signing up",Toast.LENGTH_LONG).show();
            }


            else
            {

                UserSharedPreferences.setUserId(SignUpActivity.this, userId);
                Intent intent = new Intent(SignUpActivity.this,Home.class);
                startActivity(intent);

            }



            Log.v(TAG, "The user id is " + userId);

        }
    }








}










