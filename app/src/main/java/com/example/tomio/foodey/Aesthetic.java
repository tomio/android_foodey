package com.example.tomio.foodey;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by tomio on 3/3/2016.
 */
public class Aesthetic
{

    public String getFoodeyTitleText()
    {

        String text =
                "<font color=#33cc33>F</font>" +
                        "<font color=#FF6347>o</font>" +
                        "<font color=#FF6347>o</font>" +
                        "<font color=#9999ff>d</font>" +
                        "<font color=#33cc33>e</font>" +
                        "<font color=#ffcc00>y</font>";

        return text ;
    }

    @TargetApi(21)
    @SuppressWarnings("deprecation")
    public static void setBarColor(Activity activity)
    {
        Window window = activity.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(activity.getResources().getColor(R.color.light_purple));


    }

}
