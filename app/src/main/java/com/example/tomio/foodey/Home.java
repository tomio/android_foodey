package com.example.tomio.foodey;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.tomio.foodey.data.ResterauntData;
import com.example.tomio.foodey.data.UserSharedPreferences;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Home extends AppCompatActivity implements AdapterView.OnItemClickListener {
    public static final String TAG = Home.class.getSimpleName();

    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggleListener;
    private ListView navigationList;
    private NavigationAdapter navigationAdapter;
    private ResterauntFragment[] fragmentsArray = new ResterauntFragment[2];
    private TextView currentMonth;

    private static final int POSITION_HOME = 0;
    private static final int POSITION_FAVORITES = 1 ;
    private static final int POSITION_PAST_PICKS = 2;
    private static final int POSITION_SETTINGS = 3;

    private long userId;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {



        super.onCreate(savedInstanceState);

        long userId = UserSharedPreferences.getUserId(this);




        setContentView(R.layout.activity_home);
        setCurrentMonth();
        setBarColor();

        ResterauntFragment firstResteraunt =(ResterauntFragment) getFragmentManager().findFragmentById(R.id.fragment);
        ResterauntFragment secondResteraunt =(ResterauntFragment) getFragmentManager().findFragmentById(R.id.fragment2);

         fragmentsArray[0] = firstResteraunt;
         fragmentsArray[1] = secondResteraunt;


        GetResterauntTask task = new GetResterauntTask();
        task.execute(userId);

        secondResteraunt.setCardBackgroundColor("#33cc33");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer =(DrawerLayout) findViewById(R.id.drawer_layout);
        navigationList = (ListView) findViewById(R.id.navigation_list_view);
        navigationAdapter = new NavigationAdapter(this);
        navigationList.setAdapter(navigationAdapter);
        navigationList.setOnItemClickListener(this);

        toggleListener = new ActionBarDrawerToggle(this,drawer,toolbar,R.string.open_drawer,R.string.close_drawer)

        {
            @Override
            public void onDrawerClosed(View drawerView)
            {

                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView)
            {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void setDrawerIndicatorEnabled(boolean enable) {
                super.setDrawerIndicatorEnabled(enable);
            }
        };

        drawer.setDrawerListener(toggleListener);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {

        switch (position)
        {
            case POSITION_FAVORITES:

                startActivity( new Intent(Home.this,FavoritesActivity.class));
                break;
            case POSITION_PAST_PICKS:
                startActivity(new Intent(Home.this,PastPicksActivity.class));
                break;

            case POSITION_SETTINGS:
                startActivity(new Intent(Home.this,SettingsActivity.class));
                break ;
        }

        drawer.closeDrawers();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item    )
    {

        UserSharedPreferences.deletePreferences(this);

        startActivity(new Intent(Home.this,SignInActivity.class));

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        toggleListener.syncState();
    }



    class NavigationAdapter extends BaseAdapter
    {
        String[] navigationArray;
        private Context context;
        public NavigationAdapter(Context context)
        {

            navigationArray = context.getResources().getStringArray(R.array.array);
            this.context = context;
        }

        @Override
        public int getCount() {
            return navigationArray.length;
        }

        @Override
        public Object getItem(int position) {
            return navigationArray[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row;

            if(convertView == null)
            {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.navigation_drawer_item_layout,parent,false);
                TextView rowText = (TextView) row.findViewById(R.id.row_text);



                rowText.setText(navigationArray[position]);
            }

            else
            {
                row = convertView;
            }

            return row;
        }
    }

    private void setCurrentMonth()
    {
        currentMonth = (TextView) findViewById(R.id.text_current_month);
        Calendar c = Calendar.getInstance();
        String monthNameTitle = new SimpleDateFormat("MMMM").format(c.getTime())+"'s Picks";
        currentMonth.setText(monthNameTitle);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    public class GetResterauntTask extends AsyncTask<Long,Void,String[][]> {


        private ProgressDialog dialog = new ProgressDialog(Home.this);
        private final String TAG = GetResterauntTask.class.getSimpleName();


        @Override
        protected void onPreExecute()
        {
            //this.dialog.setMessage("Please wait");
            this.dialog.show();
        }

        @Override
        protected String[][] doInBackground(Long... params) {

            ResterauntData dataHelper = new ResterauntData();
            Log.v(TAG,"The user id in async is "+params[0]);
            String[][] resteraunts = dataHelper.getCurrentRestraunts(params[0]);

            Log.v(TAG,"The amount of resteraunts "+resteraunts.length);
            return resteraunts;


        }
        @Override
        protected void onPostExecute(String[][] resteraunts)
        {
            for(int i = 0; i < resteraunts.length; i++)
            {
                String[] resterauntInfo = resteraunts[i];
                ResterauntFragment fragment = fragmentsArray[i];

                fragment.setResterauntInformation(resterauntInfo);

            }

            this.dialog.dismiss();

        }
    }



    @TargetApi(21)
    @SuppressWarnings("deprecation")
    private void setBarColor()
    {
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(getResources().getColor(R.color.light_purple));


    }
}


