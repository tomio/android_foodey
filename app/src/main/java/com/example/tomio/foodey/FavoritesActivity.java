package com.example.tomio.foodey;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.tomio.foodey.data.ResterauntData;
import com.example.tomio.foodey.data.UserSharedPreferences;

public class FavoritesActivity extends AppCompatActivity {
    private long userId;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Aesthetic.setBarColor(this);
        userId = UserSharedPreferences.getUserId(this);

        setContentView(R.layout.activity_favorites);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        GetResterauntInfoTask task = new GetResterauntInfoTask();
        task.execute(userId);

    }

     public class GetResterauntInfoTask extends AsyncTask<Long,Void,String[][]> {


        private ProgressDialog dialog = new ProgressDialog(FavoritesActivity.this);
        private final String TAG = GetResterauntInfoTask.class.getSimpleName();


        @Override
        protected void onPreExecute()
        {
            this.dialog.setMessage("Please wait");
            this.dialog.show();
        }

        @Override
        protected String[][] doInBackground(Long... params) {

            ResterauntData dataHelper = new ResterauntData();
            String[][] resteraunts = dataHelper.getFavoritedReserauntsInfo(params[0]);
            Log.v(TAG,"the user id is "+params[0]);

            Log.v(TAG,"The amount of resteraunts "+resteraunts.length);
            return resteraunts;


        }
        @Override
        protected void onPostExecute(String[][] resteraunts)
        {
            if(resteraunts.length > 0)
            {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                for(int i = 0; i < resteraunts.length; i++)
                {
                    String[] resterauntInfo = resteraunts[i];

                    Bundle bundle = new Bundle();
                    bundle.putStringArray(ResterauntData.KEY_RESTERAUNT_INFO, resterauntInfo);
                    ResterauntInfoFragment fragment = new ResterauntInfoFragment();
                    fragment.setArguments(bundle);
                    fragmentTransaction.add(R.id.resteraunt_info_container, fragment);

                    //fragment.setResterauntInfo(resterauntInfo);

                }
                fragmentTransaction.commit();


            }
            this.dialog.dismiss();

        }
    }

}
